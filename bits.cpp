//
//  bits.cpp
//  
//
//  Created by gandolf on 8/20/13.
//
//

#include <iostream>
#include <cstdlib>
#include <limits.h>
using namespace std;

int main(int argc, char** argv)
{   /*
    unsigned int i = 0x12345678;
    //unsigned char mask = (i & 0xFF);
    unsigned char byte_ = (1 << 8) & i;
    printf("byte: %0x\n", byte_);
     */
    
    //clear low 16 bits
    cout << "char bit: "<< CHAR_BIT << endl;
    unsigned int j = 0x12345678;
    //flips mask to 0x0000fffff (type safe(portable) method) mask = 0x0000ffff works the same, but assumes 32 bits
    unsigned int mask = ~0xaaaa;
    printf("mask is %.08x\n", mask);
    j &= mask;
    printf("clear low 16 bits: %.08x \n", j);
    
    
    //setting the low 16 bits.
    j = 0x12345678;
    //instead use logical OR
    mask = 0xffff;
    j |= mask;
    printf("set low 16 bits -> %.08x\n", j );
    
    
    
    //low 8 bits
    j = 0x12345678;
    printf("~j -> %.08X\n", ~j);
    unsigned char lsb = 0xff;
    unsigned char m = j & lsb;
    printf("m is -> %0X\n", m);
    
    //setting a dynamic bit position.
    // binary 0001 1010 0001 1010 0010 1010 0010 1010
    unsigned int i = 0x1A1A2A2A;
    // we define the bit we want with (1<<7)
    // start at first bit, and shift that 7 bits to the left. which places 1 at bit
    // position 7, then AND with the bit at bp 7 of i.
    int b = i & (1<<4);
    printf("value at bit position 4 -> %.08X\n", b);
    
    
    
    return 0;
}
