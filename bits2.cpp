#include <cstdlib>
#include <iostream>
#include <vector>
#include <map>

using namespace std;

map<char,vector<unsigned char>> dict;

int genmap()
{
    vector<unsigned char> vals;
    for (int i = 0; i < 10; ++i) {
        vals.push_back(i+3);
        dict['a'+i] = vals;
    }
    return 0;
}

void printmap(map<char, vector<unsigned char>>& t)
{
    for(auto mapit = t.begin(); mapit != t.end(); ++mapit)
    {
        cout<< mapit->first<< " : ";
        for(auto c: mapit->second)
        {
            //cout doesnt work, printf however does.
            printf("%i ", c);
            //cout<< c << " ";
        }
        printf("\n");
    }
    printf("\n");
}

int main(int argc, char** argv)
{
    
    genmap();
    printmap(dict);
    
    return 0;
}